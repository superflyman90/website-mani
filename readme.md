# Integration continue d'un site Symfony sous docker

suivi du tuto de "yoandev"

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose

Vous pouvez vérifier les pré-requis (sauf Docker et docker-compose) avec la commande suivante:

'''bash
symfony check:requirements
'''

### Lancer l'environnement de développement 
'''bash
docker-compose -d
symfony serve -d
'''

## Lancer des tests
'''bash
php bin/phpunit --testdox
'''